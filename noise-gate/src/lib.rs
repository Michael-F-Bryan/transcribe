#[derive(Debug, Clone, PartialEq)]
pub struct NoiseGate<S> {
    pub threshold: S,
    /// The number of samples below `noise_threshold` before we declare the
    /// audio to be "silent".
    pub hold_length: usize,
    buffer: Vec<S>,
}
